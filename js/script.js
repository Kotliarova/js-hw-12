// Чому для роботи з input не рекомендується використовувати клавіатуру?
// Тому що існує безліч варіантів як без клавіатури заповнити input, наприклад голосовим вводом тексту, скопіювати та вставити, автозаповнення тощо

document.addEventListener('keydown', ev => {
    let btn = ev.code;
    const btnName = document.querySelectorAll('[data-name]');
    btnName.forEach(elem => {
        elem.style.backgroundColor = '';
        if(btn === elem.dataset.name) {
            elem.style.backgroundColor = 'blue';
        }
    })
})